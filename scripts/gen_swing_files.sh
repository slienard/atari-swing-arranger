#!/bin/sh

set -e

generator=$(dirname "$0")/gen_swing_files.pl

$generator --gm GM \
	files/GM_Patches.csv \
	files/GM_Drums.csv

$generator GM2 \
	files/GM2_Patches.csv \
	files/GM2_Drums.csv

$generator --no-tgm SRX-02 \
	files/SRX-02_Patches.csv

$generator --no-tgm SRX-07 \
	files/SRX-07_Patches.csv \
	files/SRX-07_Drums.csv

$generator --no-tgm SRX-11 \
	files/SRX-11_Patches.csv

$generator --groups-names XV-2020 \
	files/GM2_Patches.csv \
	files/XV-2020_Patches.csv \
	files/GM2_Drums.csv \
	files/XV-2020_Drums.csv

$generator --groups-names XV-5050 \
	files/GM2_Patches.csv \
	files/XV-5050_Patches.csv \
	files/GM2_Drums.csv \
	files/XV-5050_Drums.csv

$generator --groups-names SonicCel \
	files/GM2_Patches.csv \
	files/SonicCell_Patches.csv \
	files/GM2_Drums.csv \
	files/SonicCell_Drums.csv \

mv *.TBR swing/timbres/
mv *.TGM swing/table_gm/
mv *.PRS swing/presets/
mv *.IDX swing/index/
