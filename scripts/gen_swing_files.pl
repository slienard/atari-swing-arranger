#!/usr/bin/perl
use strict;
use warnings;
use autodie;

use Getopt::Long;
use Text::Iconv;

my $groups_names = 0;
my $gm_mode = 0;
my $no_tgm = 0;
GetOptions(
	'groups-names'	=> \$groups_names,
	'gm'			=> \$gm_mode,
	'no-tgm'		=> \$no_tgm,
) or die("Command line error\n");

my $BASENAME = shift(@ARGV);
my @FILES = @ARGV;

# All "timbres" lines (not the header) must be space padded to 12 characters
my $tbr_width = 12;
# All files must use DOS type line breaks (CRLF, \r\n)
my $line_break = "\r\n";

# Which patches should be selected for GM mapping
my $gm_msb = 121;
my $gm_lsb = 0;

# Preset MIDI messages and header (below)
my $prs_messages;
my @PRS;
if ($gm_mode) {
	@PRS = ('PRS2', '1', '0', '0', '0');
	$prs_messages = 'C0%02X';
} else {
	@PRS = ('PRS2', '1', '6', '0', '0');
	$prs_messages = 'B000%02X20%02XC0%02X';
}

# Other files headers
my @IDX = ('IDX2');
my @TBR = ('TBR2');
my @TGM = ('TGM2');

my %categories = (
	# Piano
	'GM_PNO'	=> 1,
	'PNO'		=> 1,
	'AC.PIANO'	=> 1,
	'EP'		=> 1,
	'EL.PIANO'	=> 1,
	# Chromatic Percussion
	'GM_CHR'	=> 2,
	'KEY'		=> 2,
	'KEYBOARDS'	=> 2,
	'BELL'		=> 2,
	'BEL'		=> 2,
	'MALLET'	=> 2,
	'MLT'		=> 2,
	# Organ
	'GM_ORG'	=> 3,
	'ORGAN'		=> 3,
	'ORG'		=> 3,
	'ACCRDION'	=> 3,
	'ACD'		=> 3,
	'HARMONICA'	=> 3,
	'HRM'		=> 3,
	# Guitar
	'GM_GTR'	=> 4,
	'AC.GUITAR'	=> 4,
	'AGT'		=> 4,
	'EL.GUITAR'	=> 4,
	'EGT'		=> 4,
	'DIST.GUITAR'	=> 4,
	'DGT'		=> 4,
	# Bass
	'GM_BS'		=> 5,
	'BASS'		=> 5,
	'BS'		=> 5,
	'SYNTH BASS'	=> 5,
	'SBS'		=> 5,
	# Orchestra Solo
	'GM_OS'		=> 6,
	'STRINGS'	=> 6,
	'STR'		=> 6,
	# Orchestra Ensemble
	'GM_OE'		=> 7,
	'ORCHESTRA'	=> 7,
	'ORC'		=> 7,
	'HIT&STAB'	=> 7,
	'HIT'		=> 7,
	'OCH'		=> 7, # On XV-5050 panel
	# Brass
	'GM_BRS'	=> 8,
	'AC.BRASS'	=> 8,
	'BRS'		=> 8,
	'SYNTH BRASS'	=> 8,
	'SBR'		=> 8,
	# Reed
	'GM_REED'	=> 9,
	'SAX'		=> 9,
	# Wind
	'GM_WND'	=> 10,
	'WIND'		=> 10,
	'WND'		=> 10,
	'FLUTE'		=> 10,
	'FLT'		=> 10,
	# Synth Lead
	'GM_SLD'	=> 11,
	'HARD LEAD'	=> 11,
	'HLD'		=> 11,
	'SOFT LEAD'	=> 11,
	'SLD'		=> 11,
	'TECHNO SYNTH'	=> 11,
	'TEK'		=> 11,
	'PULSATING'	=> 11,
	'PLS'		=> 11,
	# Synth Pad
	'GM_PAD'	=> 12,
	'BRIGHT PAD'	=> 12,
	'BPD'		=> 12,
	'SOFT PAD'	=> 12,
	'SPD'		=> 12,
	'VOX'		=> 12,
	# Synth Sound FX
	'GM_SFX'	=> 13,
	'FX'		=> 13,
	'SYN'		=> 13,
	'OTHER SYNTH'	=> 13,
	'SYNTH FX'	=> 13,
	# Ethnic
	'GM_ETH'	=> 14,
	'PLUCKED'	=> 14,
	'PLK'		=> 14,
	'ETHNIC'	=> 14,
	'ETH'		=> 14,
	'FRETTED'	=> 14,
	'FRT'		=> 14,
	# Percussive
	'GM_PERC'	=> 15,
	'PERCUSSION'	=> 15,
	'PRC'		=> 15,
	'BEAT&GROOVE'	=> 15,
	'BTS'		=> 15,
	'COMBINATION'	=> 15,
	'CMB'		=> 15,
	'DRM'		=> 15,
	# Sound Effect
	'GM_FX'		=> 16,
	'SOUND FX'	=> 16,
	'SFX'		=> 16,
	# Drums
	'GM_DRUMS'	=> 17,
	'DRUMS'		=> 17,
);

# Read all input files
my @lines;
foreach my $file (@FILES) {
	open my $handle, '<', $file;
	chomp(my @_lines = <$handle>);

	# Ignore header
	shift @_lines;

	push(@lines, @_lines);
	close $handle;
}

# CP437 is mostly compatible with Atari ST character set
# Using "recode AtariST" might be better
my $converter = Text::Iconv->new("UTF-8", "CP437");

my $inst = 0;
my $cat = 0;
my @groups = ('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
my %IDXS;

foreach (@lines) {
	chomp();
	my ($group, $msb, $lsb, $pc, $number, $name, $category, $etc) = split(/;/, $_);

	# Name
	if (length($name) > $tbr_width) {
		warn("\"$name\" is longer than $tbr_width characters.\n");
	}
	# Comma (,) seems to be a valid names separator (like \r\n) and so needs to
	# be replaced
	$name =~ s/,/_/g;

	if (exists($categories{$category})) {
		$cat = $categories{$category};
	} else {
		die("Unknown category $category\n");
	}

	if ($groups_names and $group ne $groups[$cat]) {
		# Show group 
		$groups[$cat] = $group;
		if ($gm_mode) {
			push(@PRS, sprintf($prs_messages, $pc));
		} else {
			push(@PRS, sprintf($prs_messages, $msb, $lsb, $pc));
		}
		push(@TBR, sprintf("%-${tbr_width}s", &center($tbr_width, $converter->convert($group))));
		push(@{$IDXS{$cat}}, $inst);
		$inst++;
	}

	# PRS, TBR and IDX
	if ($gm_mode) {
		push(@PRS, sprintf($prs_messages, $pc));
	} else {
		push(@PRS, sprintf($prs_messages, $msb, $lsb, $pc));
	}
	push(@TBR, sprintf("%-${tbr_width}s", $converter->convert($name)));
	push(@{$IDXS{$cat}}, $inst);

	# TGM: GM(2)
	if (!$no_tgm and scalar @TGM < 129 and ($gm_mode or ($msb == $gm_msb and $lsb == $gm_lsb))) {
		# Map GM1 sounds (first GM2, lsb = 0) to GM Table
		push(@TGM, $inst);
	}

	$inst++;
}

my $fh;

# IDX
open($fh, ">$BASENAME.IDX") || die;
print $fh join($line_break, @IDX) . $line_break;
my $idx = 0;
print $fh $idx . $line_break;
foreach my $i (1..17) {
	if (exists($IDXS{$i})) {
		$idx += scalar @{$IDXS{$i}};
	}
	print $fh $idx . $line_break;
}
foreach my $i (1..17) {
	if (exists($IDXS{$i})) {
		print $fh join($line_break, @{$IDXS{$i}} ) . $line_break;
	}
}
close($fh);

open($fh, ">$BASENAME.PRS") || die;
print $fh join($line_break, @PRS) . $line_break;
close($fh);

open($fh, ">$BASENAME.TBR") || die;
print $fh join($line_break, @TBR) . $line_break;
close($fh);

unless ($no_tgm) {
	open($fh, ">$BASENAME.TGM") || die;
	print $fh join($line_break, @TGM) . $line_break;
	close($fh);
}

### FUNCTIONS ###

sub center () {
	my $width = shift;
	my $string = shift;

    return ( ' ' x ( ( $width - length $string ) / 2 ) ) . $string;
}
