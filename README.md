# atari-swing-arranger

Files for the Real-time MIDI arranger "Swing" on Atari ST/TT/Falcon.

See https://www.yaronet.com/topics/165536-swing-arrangeur-temps-reel

![snapshot1](img/snap1.png)

## Content

Instruments files:
* XV-2020: Roland XV-2020
* XV-5050: Roland XV-5050
* SonicCel: Roland SonicCell
* SRX-02: Roland SRX-02 (Concert Piano) XV extension card
* SRX-07: Roland SRX-07 (Ultimate Keys) XV extension card
* SRX-11: Roland SRX-11 (Complete Piano) XV extension card
* GM: General Midi (Instruments names from Roland XV-2020)
* GM2: General Midi, Level 2 (Instruments names from Roland XV-2020)

INI Files (should be renamed to SWING.INI):
* GM.INI: GM System On system exclusive message
* GM2.INI: GM2 System On system exclusive message

Scripts:
* gen_swing_files.pl: generate Swing files from CSV files describing instruments' patches and drums sets.
* gen_swing_files.sh: commands used to generate provided Swing files.

## How to use

* Get Swing from [here](https://www.yaronet.com/topics/165536-swing-arrangeur-temps-reel/3#post-75).
* Extract files on your Atari ST/TT/Falcon.
* Add files from the "swing" directory of this project.

## How to generate custom files

If you have an expander with extension cards, you can generate the Swing files combining all instruments.

Here is an example command to generate files named "XV-2020+" for Roland XV-2020 with SRX-11 and SRX-07 extension cards:
```sh
$ scripts/gen_swing_files.pl --groups-names XV-2020+ \
    files/GM2_Patches.csv \
    files/XV-2020_Patches.csv \
    files/SRX-11_Patches.csv \
    files/SRX-07_Patches.csv \
    files/GM2_Drums.csv \
    files/XV-2020_Drums.csv \
    files/SRX-07_Drums.csv
```
